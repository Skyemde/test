var gulp    = require('gulp');
var jasmine = require('gulp-jasmine-browser');

gulp.task('jasmine:ci', function() {
     return gulp.src(['src/**/*', 'spec/**/*'])
         .pipe(jasmine.specRunner({xml: true}))
         .pipe(jasmine.phantomjs({xml: true}));
});

gulp.task('jasmine:dev', function() {
     return gulp.src(['src/**/*', 'spec/**/*'])
         .pipe(jasmine.specRunner({console: true}))
         .pipe(jasmine.phantomjs({console: true}));
});

gulp.task('default', ['jasmine:ci']);
gulp.task('default', ['jasmine:dev']);
